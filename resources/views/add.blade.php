@extends('layouts.app')

@section('content')

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

<form method="POST" action="{{('AddController@update')}}">
  <fieldset>
    <div class="form-group has-feedback">
      <label for="inputCommand" class="control-label">
        <span class="label label-danger">必須項目</span>  コマンド
      </label>
      <input type="text" class="form-control" id="inputCommand" placeholder="Please input command" data-required-error="コマンドの入力は必須です" required>
      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
      <div class="help-block with-errors"></div>
    </div>

    <div class="form-group">
      <label for="input">
        <span class="label">カテゴリー</span>
      </label>
      <input type="text" class="form-control" id="input" placeholder="Please input category">
    </div>
    <p>
      <label for="textarea">
        <span class="label">説明</span>
      </label>
      <textarea id="textarea" class="form-control" rows="3"></textarea>
    </p>
    <p>
      <input type="button" value="戻る" onClick="location.href='{{ url('home') }}'">
      <button type="submit">登録</button>
    </p>
  </fieldset>
</form>


@endsection
