@extends('layouts.app')

@section('content')

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

<!--JavaScript-->
<script src="{{ asset('assets/js/home.js') }}"></script>

<table class="table table-hovertable-bordered">
    <thead>
      <tr>
        <th scope="col" class="text-left">コピーボタン</th>
        <th scope="col" class="text-center">コード</th>
        <th scope="col" class="text-center">メモ</th>
        <th scope="col" class="text-center">カテゴリー</th>
      </tr>
    </thead>
    <tbody>
    @if ($data != '')
      @php
        $dataCount=0;
      @endphp
        @foreach($data as $d)
          @php
            $dataCount++;
          @endphp
        <tr>
          <td scope="row" class="text-left"><button type="button" class="btn btn-primary btn-sm rounded-pill" onclick="copyCommand({{$dataCount}});">コマンドコピー</button></td>
          <td scope="row" class="text-center" id="targetCommand{{$dataCount}}">{{$d->command}}</td>
          <td scope="row" class="text-center">{{$d->description}}</td>
          <td scope="row" class="text-center">{{$d->category}}</td>
          <td scope="row" class="text-center"><button type="button" class="btn btn-primary btn-sm rounded-pill">編集</button></td>
        </tr>
      @endforeach
    @endif
    </tbody>
</table>
@endsection
