function copyCommand(dataCount){
  try
  {
    var idName = 'targetCommand' + dataCount;
    var copyText = document.querySelector('#' + idName);
    var range = document.createRange();
    range.selectNodeContents(copyText);

    var selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);

    if(document.execCommand('copy')) {
      alert(document.getElementById(idName).textContent + "をコピーしました");
    } else {
      alert('クリップボードへのコピーに失敗しました');
    }
  }
  catch
  {
    alert('コマンドの取得に失敗しました');
  }
}
