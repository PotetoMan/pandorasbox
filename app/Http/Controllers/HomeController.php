<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\frameworks;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function model()
    {
      // Frameworksモデルのインスタンス化
      $md = new Frameworks();
      // データ取得
      $data = $md->getData();
      // ビューを返す
      return view('home', ['data' => $data]);
    }
}
