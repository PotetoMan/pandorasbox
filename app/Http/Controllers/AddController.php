<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\frameworks;

class AddController extends Controller
{
  public function update(Request $request) {
    //セッションから取得
    $registData = $request->session()->get('article');

    //DBの更新
    $registData->save();

    //ビューの表示
    return view('home');
  }

  public function complete(Request $request) {
    return view('complete');
  }
}
